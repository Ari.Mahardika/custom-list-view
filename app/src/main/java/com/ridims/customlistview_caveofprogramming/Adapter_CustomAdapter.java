package com.ridims.customlistview_caveofprogramming;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created on 30/08/2016 by
 * Name     : Ari Mahardika Ahmad Nafis
 * Email    : arimahardika.an@gmail.com
 */
public class Adapter_CustomAdapter extends BaseAdapter {

    String[] result;
    Context context;
    int[] imageId;

    public Adapter_CustomAdapter(MainActivity mainActivity,
                                 String[] programList,
                                 int[] programImage) {
        result = programList;
        context = mainActivity;
        imageId = programImage;
    }

    @Override
    public int getCount() {
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    public class ViewHolder {
//        TextView tv;
//        ImageView iv;
//    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        ViewHolder holder = new ViewHolder();
        View rowView = convertView;

        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.design_list_row, null);
        }

        TextView tv = (TextView) rowView.findViewById(R.id.tv_row_locationName);
        ImageView iv = (ImageView) rowView.findViewById(R.id.imageview_row);

        tv.setText(result[position]);
        iv.setImageResource(imageId[position]);


//        holder.tv = (TextView)rowView.findViewById(R.id.tv_row_locationName);
//        holder.iv = (ImageView)rowView.findViewById(R.id.imageview_row);
//
//        holder.tv.setText(result[position]);
//        holder.iv.setImageResource(imageId[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You Clicked : " + result[position], Toast.LENGTH_SHORT).show();
            }
        });

        return rowView;
    }
}
